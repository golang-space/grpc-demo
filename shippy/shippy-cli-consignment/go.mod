module peterlxb/shippy/shippy-ci-consignment

go 1.15

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

require (
	github.com/micro/go-micro/v2 v2.9.1-0.20200723075038-fbdf1f2c1c4c
	google.golang.org/grpc v1.41.0
	peterlxb/shippy/shippy-service-consignment v0.0.0-00010101000000-000000000000
)

replace peterlxb/shippy/shippy-service-consignment => ../shippy-service-consignment
